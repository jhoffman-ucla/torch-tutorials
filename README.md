# Torch Tutorials

This repo contains a few examples of using pytorch to do some basic deep learning.

## MNIST

This is just basically the original MNIST version of the [pytorch getting started tutorial](https://pytorch.org/tutorials/beginner/basics/intro.html).

Contains implementations of a basic perceptron as well as a feeble convolutional neural net.

### Observations
- sigmoid activations fail to train
- initializing layers with zero weights fails to train (could be side effect of using ReLu?)
- random normal starting weights seem best
- additional hidden layers seem to hurt performance
- Learning rates
  - 0.1 seems to work(?) likely that we pretty quickly get into over-fitting territory. Max Accuracy (25 epochs): 98.2%
  - 0.01 trains up to ~90% accuracy after 2-3 epochs. Max Accuracy (25 epochs): 96.8%
  - 0.001 trains up to ~90% accuracy in 20-21 epochs. Max Accuracy (25 epochs): 90.3% (seems least likely to be overfit?)
  - 0.0001 did not train up to 90% accuracy within 50 epoch. Max Accur (50 epochs): 73.8%
- Nodes in hidden layer (learning rate of 0.001):
  - 512 works. Max accuracy (25 epochs): 90.3%
  - 128 works. Max accuracy (25 epochs): 89.6%
  - 64 works. Max accuracy (25 epochs): 89.4%. Bit slow to start, but seemed to train up quickly once going.
  - 32 works. Max accuracy (25 epochs): 88.3%.
  - 16 works. Max accuracy (25 epochs): 87.3%.
  - 10 works. Max accuracy (25 epochs): 85.8%.
  - 8 (sorta) works (not expected to be good though).  Max accuracy (25 epochs): 83.5%
  - 4 does not train.
  
### Other architectures
Adding additional linear layers (beyond the "working relu stack" version) does not improve performance.  Generally speaking, it seems to just increase the training time.  My "coalescing stack" architecture *did* reach a max accuracy of 96.7%, but I suspect that the real magic number for max accuracy is likely just the total number of hidden neurons between the input layer and the output layer (for this problem at least).  I think with more training, it's likely that the 32-node-hidden-layer architecture would likely be able to reach the "maximum" possible accuracy acheivable by this network architecture.
        
### Discussion

I'd like to better understand why the sigmoid activation fails.

It's surprising to me that 8 neurons in our hidden layers seems to actually perform pretty well.  My intuition here is that visually, the features that distinguish written digits (e.g., loops, lines, bows, arcs) are fewer than the number of digits (10). Therefore we can actually successfully encode these features into a smaller number of neurons than we require for output, and then the output layer just composes those features however it needs to.

## MNIST-LeNet

MNIST classification task with the LeNet early convolutional architecture.  

**Observations**
- A bit stubborn to train, but once it gets going, usually converges in 2-3 training epochs.
- Test accuracy of ~98%

Tested with inference and my own images (code in inference.py)
        
## Cats and Dogs

Attempt to adapt the Pytorch tutorial to a cats-and-dogs dataset that I found online.  Goal was to train a network to classify the images into "cat" or "dog." 

Have not gotten this to work.  I think training data is likely too limited, but also haven't tried anything more sophisticated than a multi-layer perceptron architecture.

**Need to double check that I'm normalizing my images (may still be scaled 0-255)**

## Platform/Backend

### MNIST

On my Lambda labs laptop, CUDA is definitely faster than CPU, however on my Mac, I think that the CPU is comparable to the GPU and both are comparable to the CUDA performance on my lambda laptop.

Two caveats though:
- I don't have full confidence that the MPS backend on my Mac is working 100% correctly
- MNIST is not a particularly challenging dataset, so performance might diverge more significantly for a more challenging task/input data

