import torch
import sys
import torch.nn as nn
from torchvision.io import read_image, ImageReadMode

class LeNet(nn.Module):
    def __init__(self):
        super(LeNet, self).__init__()

        self.encode = nn.Sequential(
            nn.Conv2d(1, 6, 5, stride=1, padding=2),
            nn.AvgPool2d(2, stride = 2),

            nn.Sigmoid(),

            nn.Conv2d(6, 16, 5),
            nn.AvgPool2d(2, stride = 2),

            nn.Sigmoid(),
            
            nn.Flatten(1),
            nn.Linear(400, 120),
            nn.Sigmoid(),
            nn.Linear(120, 84),
            nn.Sigmoid(),
            nn.Linear(84,10),
        )
        
    def forward(self, x):
        return self.encode(x)

# Load the inference model
model = torch.load("model_03.pt")
model.eval()

# Load the image
#torch.zeros(1,1,28,28)
with torch.no_grad():
    img = read_image(sys.argv[1], ImageReadMode.GRAY)/255.0
    img = torch.unsqueeze(img, 0)
    
    print(f"logits: {model(img)}")
    print(f"Final guess: {model(img).argmax(1)}")
