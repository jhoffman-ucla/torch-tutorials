import os
import sys
import torch
from torch import nn

import torchvision.datasets as datasets
import torchvision.transforms as transforms

from torch.utils.data  import DataLoader

device = "cpu"
dim_latent_space = 10
conv_channels = 16

class NeuralNetwork(nn.Module):
    def __init__(self):
        super(NeuralNetwork, self).__init__()

        self.encode_stack = nn.Sequential(
            # Convolution stack 1
            nn.Conv2d(1, conv_channels, 3, stride=1), # 28x28 -> 26x26
            nn.ReLU(),
            nn.Conv2d(conv_channels, conv_channels, 3, stride=1), # 26x26 -> 24x24
            nn.ReLU(),
            nn.MaxPool2d(2), # 24->12

            # Convolution stack 2
            nn.Conv2d(conv_channels, conv_channels, 3, stride=1), # 12 -> 10
            nn.ReLU(),
            nn.Conv2d(conv_channels, 1, 3, stride=1), # 10 -> 8
            nn.ReLU(),
            nn.MaxPool2d(2), # 8->4

            # Flatten, fully-connected
            nn.Flatten(),
            nn.Linear(16, dim_latent_space),
            #nn.ReLU(),
        )

    def forward(self, x):
        logits = self.encode_stack(x)
        return logits

def train_loop(dl, model, loss_fn, optimizer):
    size = len(dl.dataset)
    for batch, (X, y) in enumerate(dl):

        X = X.to(device)
        y = y.to(device)
        
        # compute prediction and loss
        # Loss here is the difference between the encoded->decoded result and the original input image
        pred = model(X)
        loss = loss_fn(pred, y) 

        # Backpropagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if batch % 100 == 0:
            loss, current = loss.item(), batch * len(X)
            print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")

def test_loop(dl, model, loss_fn):
    size = len(dl.dataset)
    num_batches = len(dl)
    test_loss, correct = 0, 0
    
    with torch.no_grad():
        for X, y in dl:
            X = X.to(device)
            y = y.to(device)
            
            pred = model(X)
            test_loss += loss_fn(pred, y).item()

            #print(pred)
            #print(pred.argmax(1))
            #print(y.argmax(1))

            test = (pred.argmax(1) == y.argmax(1))
            correct += (test).type(torch.float).sum().item()

    test_loss /= num_batches
    correct /= size
    
    print(f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Avg loss: {test_loss:>8f} \n")

def one_hot(y):
    return torch.zeros(10, dtype=torch.float).scatter_(0, torch.tensor(y), value=1)

def main():

    # Instatiate data loaders
    data_train = datasets.MNIST(
        root="/Users/jhoffman/Data",
        train = True,
        download=True,
        transform = transforms.ToTensor(),
        target_transform = transforms.Lambda(one_hot))
        
    data_test = datasets.MNIST(
        root="/Users/jhoffman/Data",
        train = False,
        download=True,
        transform = transforms.ToTensor(),
        target_transform = transforms.Lambda(one_hot))

    kwargs = {'num_workers': 1, 'pin_memory': True} if (device == "cuda") else {}

    # Hyperparameters
    learning_rate = 1e-3
    batch_size = 64
    epochs = 100

    train_dataloader = DataLoader(data_train, batch_size = batch_size, shuffle = True, **kwargs)
    test_dataloader = DataLoader(data_test, batch_size = batch_size, shuffle = True, **kwargs)

    model = NeuralNetwork().to(device)
    print(model)

    # Train
    loss_fn = nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)
    
    for t in range(epochs):
        print(f"Epoch {t}:")
        train_loop(train_dataloader, model, loss_fn, optimizer)
        test_loop(test_dataloader, model, loss_fn)

if __name__=="__main__":    
    main()

