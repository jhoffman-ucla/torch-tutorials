import os
import sys
import torch
from torch import nn
import pandas as pd
import matplotlib.pyplot as plt

from torchvision import datasets
from torch.utils.data  import Dataset, DataLoader
from torchvision import datasets, transforms
from torchvision.transforms import Lambda, ToTensor, Resize, Grayscale, Compose, ConvertImageDtype
from torchvision.io import read_image

target_img_size = 28

device = "cuda" if torch.cuda.is_available() else "cpu"
use_cuda = True if torch.cuda.is_available() else False
print(f"Using {device} device")

def LinearZero(n, m):
    l = nn.Linear(n, m)
    
    print(l.weight)
    print(l.weight.size())
    
    l.weight = torch.nn.Parameter(torch.zeros(m, n))
    print(l.weight)
    print(l.weight.size())
    
    #l.bias = torch.nn.Parameter(torch.ones(m))
    return l

##################################################
# Build our network
##################################################
class NeuralNetwork(nn.Module):
    def __init__(self):
        super(NeuralNetwork, self).__init__()
        self.flatten = nn.Flatten()
        # Working ReLu stack
        #self.linear_relu_stack = nn.Sequential(
        #    nn.Linear(target_img_size * target_img_size, 512),
        #    nn.ReLU(),
        #    nn.Linear(512, 512),
        #    nn.ReLU(),
        #    nn.Linear(512, 10),
        #)

        # Variable hidden neurons ReLu stack
        hidden_neurons = 32
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(target_img_size * target_img_size, hidden_neurons),
            nn.ReLU(),
            nn.Linear(hidden_neurons, hidden_neurons),
            nn.ReLU(),
            nn.Linear(hidden_neurons, 10),
        )

        # Coalescing stack
        #self.linear_relu_stack = nn.Sequential(
        #    nn.Linear(target_img_size * target_img_size, 128),
        #    nn.ReLU(),
        #    nn.Linear(128, 32),
        #    nn.ReLU(),
        #    nn.Linear(32, 8),
        #    nn.ReLU(),
        #    nn.Linear(8, 10),
        #)
        
    def forward(self, x):
        x = self.flatten(x)
        logits = self.linear_relu_stack(x)
        return logits
    
##################################################
# Visualize the data
##################################################
def visualize(data):
    labels_map = {
        0: "kitty",
        1: "doge"
    }
    figure = plt.figure(figsize=(8, 8))
    cols, rows = 3, 3
    
    for i in range(1, cols * rows + 1):
        sample_idx = torch.randint(len(data), size=(1,)).item()
        img, label = data[sample_idx]
        figure.add_subplot(rows, cols, i)
        plt.title(str(label))
        plt.axis("off")
        
        print(img.size(dim=1), img.size(dim=2))
        
        plt.imshow(img.numpy().squeeze(), cmap="gray")

    plt.show()

def predict(model, img):
    logits = model(img)
    pred_probab = nn.Softmax(dim=1)(logits)
    y_pred = pred_probab.argmax(1)
    print(f"predicted {y_pred}")

def train_loop(dl, model, loss_fn, optimizer):
    size = len(dl.dataset)
    for batch, (X, y) in enumerate(dl):

        X = X.to(device)
        y = y.to(device)
        
        # compute prediction and loss
        pred = model(X)
        loss = loss_fn(pred, y)

        # Backpropagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if batch % 100 == 0:
            loss, current = loss.item(), batch * len(X)
            print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")

def test_loop(dl, model, loss_fn):
    size = len(dl.dataset)
    num_batches = len(dl)
    test_loss, correct = 0, 0
    
    with torch.no_grad():
        for X, y in dl:
            X = X.to(device)
            y = y.to(device)
            
            pred = model(X)
            test_loss += loss_fn(pred, y).item()

            #print(pred)
            #print(pred.argmax(1))
            #print(y.argmax(1))

            test = (pred.argmax(1) == y.argmax(1))
            correct += (test).type(torch.float).sum().item()

    test_loss /= num_batches
    correct /= size
    
    print(f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Avg loss: {test_loss:>8f} \n")
            
def main():
    # Instatiate data loader
    data_train = datasets.MNIST(
        root="/home/john/Data",
        train = True,
        download=True,
        transform = ToTensor(),
        target_transform = Lambda(lambda y: torch.zeros(10, dtype=torch.float).scatter_(0, torch.tensor(y), value=1)))

    data_test = datasets.MNIST(
        root="/home/john/Data",
        train = False,
        download=True,
        transform = ToTensor(),
        target_transform = Lambda(lambda y: torch.zeros(10, dtype=torch.float).scatter_(0, torch.tensor(y), value=1)))


    kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}

    train_dataloader = DataLoader(data_train, batch_size = 64, shuffle = True, **kwargs)
    test_dataloader = DataLoader(data_test, batch_size = 64, shuffle = True, **kwargs)

    #visualize(data_train)

    # Create our model
    model = NeuralNetwork().to(device)
    print(model)

    # Hyperparameters
    learning_rate = 1e-3
    batch_size = 64
    epochs = 100

    # Train
    loss_fn = nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)

    for t in range(epochs):
        print(f"Epoch {t}:")
        train_loop(train_dataloader, model, loss_fn, optimizer)
        test_loop(test_dataloader, model, loss_fn)

if __name__=="__main__":    
    main()

