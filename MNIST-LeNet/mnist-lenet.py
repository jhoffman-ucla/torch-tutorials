import os
import sys

import numpy as np

import torch
from torch import nn

import torchvision.datasets as datasets
import torchvision.transforms as transforms

from torch.utils.data  import DataLoader

#data_path = "/home/john/Data/"
data_path = "/Users/jhoffman/Data/"
device = "cpu"

print(f"Using {device} backend")

# A "nothing-fancy" implementation of LeNet-5 for MNist
def get_run_index():
    files = [f for f in os.listdir(".") if (os.path.isfile(f) and (".pt" in f))]
    return len(files)

def get_save_path():
    return "model_{:02d}.pt".format(get_run_index())


class EarlyStoppingManager():
    def __init__(self, min_epochs=10, patience=3, tolerance = 0):
        self.min_epochs = min_epochs
        self.patience = patience
        self.tolerance = tolerance
        self.history = []
        self.counter = 0

    def record_and_check(self, validation_loss):

        self.history.append(validation_loss)

        trigger_early_stop = False

        # Don't stop if we haven't trained for the minimum number of epochs just yet
        if len(self.history) < self.min_epochs:
            return trigger_early_stop

        # Compute the rolling average change over
        interval = self.history[-self.patience:]
        rolling_change = np.mean(np.diff(interval))
        if rolling_change > self.tolerance:
            print(f"Early stopping: {rolling_change} > {self.tolerance} (stopping!)" )
            trigger_early_stop = True
        else:
            print(f"Early stopping: {rolling_change} <= {self.tolerance} (continuing)" )

        return trigger_early_stop
        
class LeNet(nn.Module):
    def __init__(self):
        super(LeNet, self).__init__()

        self.encode = nn.Sequential(
            nn.Conv2d(1, 6, 5, stride=1, padding=2),
            nn.AvgPool2d(2, stride = 2),

            nn.Sigmoid(),

            nn.Conv2d(6, 16, 5),
            nn.AvgPool2d(2, stride = 2),

            nn.Sigmoid(),
            
            nn.Flatten(1),
            nn.Linear(400, 120),
            nn.Sigmoid(),
            nn.Linear(120, 84),
            nn.Sigmoid(),
            nn.Linear(84,10),
        )
        
    def forward(self, x):
        return self.encode(x)

def train_loop(dl, model, loss_fn, optimizer):
    size = len(dl.dataset)
    for batch, (X, y) in enumerate(dl):

        X = X.to(device)
        y = y.to(device)
        
        # compute prediction and loss
        # Loss here is the difference between the encoded->decoded result and the original input image
        pred = model(X)
        loss = loss_fn(pred, y) 

        # Backpropagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if batch % 100 == 0:
            loss, current = loss.item(), batch * len(X)
            print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")

def test_loop(dl, model, loss_fn):
    size = len(dl.dataset)
    num_batches = len(dl)
    test_loss, correct = 0, 0
    
    with torch.no_grad():
        for X, y in dl:
            X = X.to(device)
            y = y.to(device)
            
            pred = model(X)
            test_loss += loss_fn(pred, y ).item()
                        
            #print(pred.size())
            #print(pred.argmax(1))
            #print(y.argmax(1))

            test = (pred.argmax(1) == y.argmax(1))
            correct += (test).type(torch.float).sum().item()

    test_loss /= num_batches
    correct /= size
    
    print(f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Avg loss: {test_loss:>8f} \n")
    return test_loss

def one_hot(y):
    return torch.zeros(10, dtype=torch.float).scatter_(0, torch.tensor(y), value=1)
    
def main():

    # Instatiate data loaders
    data_train = datasets.MNIST(
        root = data_path,
        train = True,
        download=True,
        transform = transforms.ToTensor(),
        target_transform = transforms.Lambda(one_hot))
    
    data_test = datasets.MNIST(
        root= data_path,
        train = False,
        download=True,
        transform = transforms.ToTensor(),
        target_transform = transforms.Lambda(one_hot))

    kwargs = {"num_workers": 1, "pin_memory": True} if (device == "cuda") else {}

    # Hyperparameters
    #learning_rate = 1e-1
    learning_rate = 1e0
    batch_size = 32
    epochs = 100

    train_dataloader = DataLoader(data_train, batch_size = batch_size, shuffle = True, **kwargs)
    test_dataloader = DataLoader(data_test, batch_size = batch_size, shuffle = True, **kwargs)

    model = LeNet().to(device)
    print(model)

    def count_parameters(model):
        return sum(p.numel() for p in model.parameters() if p.requires_grad)
    
    n_param = count_parameters(model)
    print(f"Model has {n_param} tuneable parameteers")

    #test = torch.randn(1,1,28,28)
    #x = model(test)
    #print(x.size())
    #sys.exit(1)
    
    # Train
    loss_fn = nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)

    early_stopper = EarlyStoppingManager(min_epochs=20, patience=3, tolerance=0)

    for t in range(epochs):
        print(f"Epoch {t}:")
        train_loop(train_dataloader, model, loss_fn, optimizer)
        test_loss = test_loop(test_dataloader, model, loss_fn)

        stop_triggered = early_stopper.record_and_check(test_loss)
        if stop_triggered:
            break

    torch.save(model, get_save_path())

if __name__=="__main__":    
    main()

